import React from "react";

const AboutPage = () => (
  <div>
    <h2>AboutUs</h2>
    <p>
      This application uses React, Redux-Thunk , Redux-Saga , React Router, and many other helpful
      libraries.
    </p>
  </div>
);

export default AboutPage;
