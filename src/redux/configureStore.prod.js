import { createStore, applyMiddleware } from "redux";
import rootReducer from "./reducers";
import thunk from "redux-thunk";
import createSagaMiddleWare from "redux-saga";
import { watchAllSagaCalls } from "./sagas/index";

export default function configureStore(initialState) {
  //create SagaMiddleware
  const sagaMiddleWare = createSagaMiddleWare();
  //Create Store
  const store = createStore(
    rootReducer,
    initialState,
    applyMiddleware(thunk, sagaMiddleWare)
  );
  //run SagaMiddleWare
  sagaMiddleWare.run(watchAllSagaCalls);
  return store;
}
