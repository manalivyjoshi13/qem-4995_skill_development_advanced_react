import { createStore, applyMiddleware, compose } from "redux";
import rootReducer from "./reducers";
import reduxImmutableStateInvariant from "redux-immutable-state-invariant";
import thunk from "redux-thunk";
import createSagaMiddleWare from "redux-saga";
import { watchAllSagaCalls } from "./sagas/index";

export default function configureStore(initialState) {
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; // add support for Redux dev tools

  //create SagaMiddleware
  const sagaMiddleWare = createSagaMiddleWare();

  const store = createStore(
    rootReducer,
    initialState,
    composeEnhancers(
      applyMiddleware(thunk, sagaMiddleWare, reduxImmutableStateInvariant())
    )
  );
  //run SagaMiddleWare
  sagaMiddleWare.run(watchAllSagaCalls);
  return store;
}
