import * as autherApi from "../../api/authorApi";

export function* getAutherSaga(action) {
    yield autherApi.getAuthors();
}

