import * as courseApi from "../../api/courseApi";
// we are genrating Genraters when we say we are creating saga
// genrater give as liberty to puse over code to handle async code

export function* deleteCourseSaga(action) {
  yield courseApi.deleteCourse(action.course.id);
}
