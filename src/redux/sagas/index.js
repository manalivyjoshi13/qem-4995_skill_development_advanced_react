import { takeEvery } from "redux-saga/effects";
import * as actionTypes from "../actions/actionTypes";
import { deleteCourseSaga } from "./coursesSaga";
import { getAutherSaga } from "./autherSaga";

export function* watchAllSagaCalls() {
    //take every help us to perform any event after any action call detect 
    yield takeEvery(actionTypes.DELETE_COURSE_INITIATE,deleteCourseSaga);
    yield takeEvery(actionTypes.GET_AUTHER_INITIATE,getAutherSaga);
}
